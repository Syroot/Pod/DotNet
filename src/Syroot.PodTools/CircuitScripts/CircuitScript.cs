using System.Buffers.Binary;
using Syroot.PodTools.Core;

namespace Syroot.PodTools.CircuitScripts;

/// <summary>
/// Represents the contents of the circuit listing file (circuits.bin).
/// </summary>
public class CircuitScript
{
    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public IList<CircuitInfo> Infos { get; set; } = [];

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static CircuitScript Load(Stream stream)
    {
        CircuitScript script = new();

        int infoCount = stream.ReadInt32();
        Span<byte> buffer = stackalloc byte[140];
        for (int i = 0; i < infoCount; i++)
        {
            stream.ReadExactly(buffer);
            for (int j = 0; j < buffer.Length; j++)
                buffer[j] ^= 0x50;

            script.Infos.Add(new()
            {
                Name = Encodings.Codepage1252.GetStringBuf(buffer.Slice(0, 20)),
                LevName = Encodings.Codepage1252.GetStringBuf(buffer.Slice(20, 64)),
                TgaName = Encodings.Codepage1252.GetStringBuf(buffer.Slice(84, 13)),
                Scotch = Encodings.Codepage1252.GetStringBuf(buffer.Slice(97, 13)),
                ScotchMirror = Encodings.Codepage1252.GetStringBuf(buffer.Slice(110, 14)),
                Flags = (CircuitFlags)buffer[124],
                Version = buffer[125],
                Id = buffer[126],
                Length = BinaryPrimitives.ReadInt32LittleEndian(buffer.Slice(128, 4)),
                Laps = buffer[132],
                Level = buffer[133]
            });
        }

        return script;
    }
}

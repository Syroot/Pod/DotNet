﻿namespace Syroot.PodTools.VehicleScripts;

public class VehicleInfo
{
    public string Name { get; set; } = "";
    public string VehicleName { get; set; } = "";
    public string TgaName { get; set; } = "";
    public string SmallEName { get; set; } = "";
    public string SmallAName { get; set; } = "";
}

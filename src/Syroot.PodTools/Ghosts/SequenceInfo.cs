namespace Syroot.PodTools.Ghosts;

public class SequenceInfo
{
    public int Unknown0 { get; set; }
    public int CircuitIndex { get; set; } // dependent on client circuit order
    public double Time { get; set; }
    public int[] Unknown1 { get; set; } = new int[7];
    public double RaceTime { get; set; }
    public double StartTime { get; set; }
    public double[] LapTimes { get; } = new double[3];
    public double[][] PartTimes { get; } = new double[3][];
    public int[] Unknown2 { get; set; } = new int[6];
    public string VehicleName { get; set; } = "";
    public int[] Unknown3 { get; set; } = new int[7];
    public string PlayerName { get; set; } = "";
    public byte[] Unknown4 { get; set; } = new byte[50];
    public byte Grip { get; set; }
    public byte Speed { get; set; }
    public byte Handling { get; set; }
    public byte Brakes { get; set; }
    public byte Accel { get; set; }
    public byte Unknown5 { get; set; }
    public int[] Unknown6 { get; set; } = new int[5];

    public SequenceInfo()
    {
        for (int i = 0; i < PartTimes.Length; i++)
            PartTimes[i] = new double[4];
    }
}

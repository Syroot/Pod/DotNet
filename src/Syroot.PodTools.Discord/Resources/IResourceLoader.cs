﻿namespace Syroot.PodTools.Discord.Resources;

public interface IResourceLoader
{
    // ---- METHODS ----------------------------------------------------------------------------------------------------

    byte[] GetData(string name);
}

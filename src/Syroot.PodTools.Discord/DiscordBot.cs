﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Options;

namespace Syroot.PodTools.Discord;

public class DiscordBot : IHostedService
{
    // ---- FIELDS -----------------------------------------------------------------------------------------------------

    private readonly DiscordSocketClient _client;
    private readonly GhostMessageHandler _ghostMessageHandler;
    private readonly ILogger _logger;
    private readonly DiscordBotOptions _options;

    // ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

    public DiscordBot(
        DiscordSocketClient client,
        GhostMessageHandler ghostMessageHandler,
        ILogger<DiscordBot> logger,
        IOptions<DiscordBotOptions> options)
    {
        _client = client;
        _ghostMessageHandler = ghostMessageHandler;
        _logger = logger;
        _options = options.Value;

        _client.Log += Client_Log;
        _client.MessageReceived += Client_MessageReceived;
    }

    // ---- METHODS ----------------------------------------------------------------------------------------------------

    async Task IHostedService.StartAsync(CancellationToken cancellationToken)
    {
        await _client.LoginAsync(TokenType.Bot, _options.Token);
        await _client.StartAsync();
    }

    async Task IHostedService.StopAsync(CancellationToken cancellationToken)
    {
        await _client.LogoutAsync();
        await _client.DisposeAsync();
    }

    // ---- EVENTHANDLERS ----------------------------------------------------------------------------------------------

    private async Task Client_Log(LogMessage message)
    {
        LogLevel level = message.Severity switch
        {
            LogSeverity.Critical => LogLevel.Critical,
            LogSeverity.Error => LogLevel.Error,
            LogSeverity.Warning => LogLevel.Warning,
            LogSeverity.Info => LogLevel.Information,
            LogSeverity.Verbose => LogLevel.Trace,
            LogSeverity.Debug => LogLevel.Debug,
            _ => throw new ArgumentException($"Unknown log severity {message.Severity}")
        };
        _logger.Log(level, message.Exception, "{Source} {Message}", message.Source, message.Message);
        await Task.CompletedTask;
    }

    private async Task Client_MessageReceived(SocketMessage message)
    {
        await _ghostMessageHandler.HandleMessageAsync(message);
    }
}

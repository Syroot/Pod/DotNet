﻿using System.Text;

namespace Syroot.PodTools.Core;

public static class Encodings
{
    // ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

    static Encodings()
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        Codepage1252 = Encoding.GetEncoding(1252);
    }

    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public static Encoding Codepage1252 { get; }
}

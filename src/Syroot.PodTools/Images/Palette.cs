﻿using System.Drawing;

namespace Syroot.PodTools.Images;

public class Palette
{
    public Color[] Colors { get; } = new Color[256];
}

﻿using System.Buffers.Binary;
using System.Drawing;
using SkiaSharp;
using Syroot.PodTools.Core;

namespace Syroot.PodTools.Images;

public static class ImageFileExtensions
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static SKImage GetSKImage(this ImageFile imageFile, int imageId)
    {
        Image image = imageFile.Images.First(x => x.Id == imageId);
        int pixelCount = image.Width * image.Height;
        byte[] pixels = new byte[pixelCount * 4];

        if (image.Palette == null)
        {
            for (int i = 0; i < pixelCount; i++)
            {
                ushort value = BinaryPrimitives.ReadUInt16LittleEndian(image.Data.AsSpan(i * 2));
                Color color = Colors.Rgb555ToColor(value);
                pixels[i * 4 + 0] = color.R;
                pixels[i * 4 + 1] = color.G;
                pixels[i * 4 + 2] = color.B;
            }
        }
        else
        {
            for (int i = 0; i < pixelCount; i++)
            {
                byte value = image.Data[i];
                Color color = image.Palette.Colors[value];
                pixels[i * 4 + 0] = color.R;
                pixels[i * 4 + 1] = color.G;
                pixels[i * 4 + 2] = color.B;
            }
        }

        return SKImage.FromPixelCopy(new SKImageInfo(image.Width, image.Height, SKColorType.Rgb888x), pixels);
    }
}

﻿using Discord;
using Discord.WebSocket;
using Syroot.PodTools.Discord;
using Syroot.PodTools.Discord.Resources;

Environment.CurrentDirectory = AppContext.BaseDirectory;

await Host.CreateDefaultBuilder(args)
    .ConfigureServices((context, services) => services
        .AddSingleton(new DiscordSocketConfig
        {
            GatewayIntents = GatewayIntents.AllUnprivileged
                & ~GatewayIntents.GuildInvites
                & ~GatewayIntents.GuildScheduledEvents
                | GatewayIntents.MessageContent
        })
        .AddHostedService<DiscordBot>()
        .AddSingleton<DiscordSocketClient>()
        .Configure<DiscordBotOptions>(context.Configuration.GetSection("Discord"))

        .AddSingleton<IGameData, GameData>()
        .Configure<GameDataOptions>(context.Configuration.GetSection("Game"))

        .AddSingleton<GhostMessageHandler>()
        .AddSingleton(new HttpClient(new SocketsHttpHandler { PooledConnectionLifetime = TimeSpan.FromMinutes(15) }))
        .AddSingleton<IGhostDetailsExtractor, GhostDetailsExtractor>()
        .AddSingleton<IImageRenderer, ImageRenderer>()
        .AddSingleton<ILiveSplitExporter, LiveSplitExporter>()
        .AddSingleton<IResourceLoader, ResourceLoader>()
    )
    .UseWindowsService()
    .Build()
    .RunAsync();

using Syroot.PodTools.Core;
using Syroot.PodTools.Ghosts;
using Syroot.PodTools.CircuitScripts;
using Syroot.PodTools.VehicleScripts;

namespace Syroot.PodTools.Discord;

public class GhostDetailsExtractor(
    IGameData _gameData) : IGhostDetailsExtractor
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public GhostDetails Create(string filename, SequenceInfo info)
    {
        GhostDetails details = new();

        // Determine circuit.
        string circuitName = Path.GetFileNameWithoutExtension(filename).Replace('_', ' ');
        if (_gameData.TryGetCircuitInfo(circuitName, out CircuitInfo? circuitInfo))
        {
            details.CircuitName = circuitInfo.Name;
            details.CircuitLevName = circuitInfo.LevName;
        }
        else if (_gameData.TryGetCircuitInfo(circuitName[..^1], out circuitInfo)) // remove 'M' suffix of mirrored circuits
        {
            details.CircuitName = circuitInfo.Name;
            details.CircuitLevName = circuitInfo.LevName;
            details.CircuitMirror = true;
        }
        else
        {
            details.CircuitName = circuitName;
            details.CircuitLevName = circuitName;
            details.CircuitUnknown = true;
        }

        // Determine vehicle.
        string vehicleName = info.VehicleName;
        if (_gameData.TryGetVehicleInfo(vehicleName, out VehicleInfo? vehicleInfo))
        {
            details.VehicleName = vehicleInfo.VehicleName.ToTitle(); // original cars are all-uppercase
        }
        else
        {
            details.VehicleName = vehicleName.ToTitle();
            details.VehicleUnknown = true;
        }
        details.VehiclePointsTotal = info.Accel + info.Brakes + info.Grip + info.Handling + info.Speed;

        // Determine best times.
        details.BestLapTime = info.LapTimes.Min();
        details.WorstLapTime = info.LapTimes.Max();
        details.PartCount = info.PartTimes[0].Count(x => x != 0);
        for (int lap = 0; lap < 3; lap++)
        {
            for (int part = 0; part < details.PartCount; part++)
            {
                double partTime = info.PartTimes[lap][part];
                if (details.BestPartTimes[part] == 0 || details.BestPartTimes[part] > partTime)
                    details.BestPartTimes[part] = partTime;
                if (details.WorstPartTimes[part] == 0 || details.WorstPartTimes[part] < partTime)
                    details.WorstPartTimes[part] = partTime;
            }
        }
        details.AverageLapTime = info.LapTimes.Sum() / info.LapTimes.Length;
        details.VirtualLapTime = details.BestPartTimes.Sum();

        return details;
    }
}

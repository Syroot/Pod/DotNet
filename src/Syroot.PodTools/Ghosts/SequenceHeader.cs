namespace Syroot.PodTools.Ghosts;

public class SequenceHeader
{
    public int Type { get; set; }
    public byte[] Data { get; set; } = [];
}

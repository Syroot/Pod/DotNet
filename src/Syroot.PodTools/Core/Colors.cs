﻿using System.Drawing;

namespace Syroot.PodTools.Core;

/// <summary>
/// Represents accurate full color-space conversions.
/// </summary>
public static class Colors
{
    // ---- FIELDS -----------------------------------------------------------------------------------------------------

    private static readonly byte[] s_bits5To8;
    private static readonly byte[] s_bits6To8;
    private static readonly byte[] s_bits8To5;
    private static readonly byte[] s_bits8To6;

    // ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

    static Colors()
    {
        static byte[] createTable(byte from, byte to)
        {
            int max = 1 << from;
            int mul = (1 << to) - 1;
            int add = (1 << from - 1) - 1;
            byte[] array = new byte[max];
            for (int i = 0; i < max; ++i)
                array[i] = (byte)((i * mul + add) / (max - 1));
            return array;
        }

        s_bits5To8 = createTable(5, 8);
        s_bits6To8 = createTable(6, 8);
        s_bits8To5 = createTable(8, 5);
        s_bits8To6 = createTable(8, 6);
    }

    // ---- METHODS (INTERNAL) -----------------------------------------------------------------------------------------

    public static Color Rgb555ToColor(ushort rgb)
    {
        return Color.FromArgb(
            s_bits5To8[rgb >> 10 & 0b11111],
            s_bits5To8[rgb >> 5 & 0b11111],
            s_bits5To8[rgb & 0b11111]);
    }

    public static Color Rgb565ToColor(ushort rgb)
    {
        return Color.FromArgb(
            s_bits5To8[rgb >> 11 & 0b11111],
            s_bits6To8[rgb >> 5 & 0b111111],
            s_bits5To8[rgb & 0b11111]);
    }

    public static ushort ColorToRgb555(Color color)
    {
        return (ushort)(s_bits8To5[color.R] << 10 | s_bits8To5[color.G] << 5 | s_bits8To5[color.B]);
    }

    public static ushort ColorToRgb565(Color color)
    {
        return (ushort)(s_bits8To5[color.R] << 11 | s_bits8To6[color.G] << 5 | s_bits8To5[color.B]);
    }
}

using Syroot.PodTools.Core;
using Syroot.PodTools.Pbdf;

namespace Syroot.PodTools.Pbdf;

/// <summary>
/// Represents extension methods for <see cref="PbdfReader"/> instances.
/// </summary>
public static class PbdfReaderExtensions
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    /// <summary>
    /// Reads a string buffer of the given <paramref name="length"/> as a <see cref="string"/> instance.
    /// </summary>
    /// <param name="pbdf">A <see cref="PbdfReader"/> to read with.</param>
    /// <param name="length">The size of the buffer the string is stored in.</param>
    /// <returns>The read <see cref="string"/> instance.</returns>
    public static string ReadStringBuf(this PbdfReader pbdf, int length)
    {
        return pbdf.ReadStringBuf(length, pbdf.Encoding);
    }

    /// <summary>
    /// Reads an encoded string as a <see cref="string"/> instance.
    /// </summary>
    /// <param name="pbdf">A <see cref="PbdfReader"/> to read with.</param>
    /// <returns>The read <see cref="string"/> instance.</returns>
    public static string ReadStringEnc(this PbdfReader pbdf)
    {
        Span<byte> buffer = stackalloc byte[pbdf.ReadUInt8()];
        pbdf.ReadExactly(buffer);
        for (int i = 0; i < buffer.Length; i++)
            buffer[i] = (byte)(buffer[i] ^ ~i);
        return pbdf.Encoding.GetString(buffer);
    }
}

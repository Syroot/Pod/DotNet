﻿namespace Syroot.PodTools.Discord;

public class DiscordBotOptions
{
    /// <summary>
    /// Gets or sets the Discord bot token to authenticate with.
    /// </summary>
    public string Token { get; set; } = "";
}

namespace Syroot.PodTools.Discord;

public interface IImageRenderer
{
    // ---- METHODS ----------------------------------------------------------------------------------------------------

    byte[] GetCircuitImage(string name, bool mirror);

    byte[] GetVehicleImage(string name);
}

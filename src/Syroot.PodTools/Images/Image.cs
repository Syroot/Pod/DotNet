﻿namespace Syroot.PodTools.Images;

public class Image
{
    public short Width { get; set; }
    public short Height { get; set; }
    public int Id { get; set; }
    public Palette? Palette { get; set; }
    public byte[] Data { get; set; } = [];
}

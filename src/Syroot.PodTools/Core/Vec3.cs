﻿namespace Syroot.PodTools.Core;

/// <summary>
/// Represents a three dimensional mathematical vector.
/// </summary>
/// <param name="x">Initial value for the X component of the vector.</param>
/// <param name="y">Initial value for the Y component of the vector.</param>
/// <param name="z">Initial value for the Z component of the vector.</param>
public readonly record struct Vec3(double x, double y, double z)
{
    /// <summary>The X component of the vector.</summary>
    public readonly double X = x;
    /// <summary>The Y component of the vector.</summary>
    public readonly double Y = y;
    /// <summary>The Z component of the vector.</summary>
    public readonly double Z = z;
}

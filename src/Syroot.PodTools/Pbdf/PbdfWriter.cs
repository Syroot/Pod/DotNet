﻿using System.Buffers.Binary;
using System.Text;
using Syroot.PodTools.Core;

namespace Syroot.PodTools.Pbdf;

/// <summary>
/// Represents a write-only POD binary data file (PBDF) stream.
/// </summary>
public class PbdfWriter : Stream, IDisposable
{
    // ---- FIELDS -----------------------------------------------------------------------------------------------------

    private readonly Stream _baseStream;
    private readonly bool _leaveOpen;
    private readonly byte[] _buffer;
    private int _bufferPos;
    private readonly int[] _offsets;
    private int _offsetIndex;

    // ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

    /// <summary>
    /// Initializes a new instance of the <see cref="PbdfWriter"/> class with the given settings.
    /// </summary>
    /// <param name="baseStream">An underlying <see cref="Stream"/> to write input data to. This stream requires to
    /// be writable and seekable.</param>
    /// <param name="key">An encryption key with which the contents of this file are encrypted.</param>
    /// <param name="bufferSize">A size of a block in the file at which a checksum follows.</param>
    /// <param name="offsetCount">The number of offsets to be satisfied while writing the file contents.</param>
    /// <param name="encoding">An <see cref="System.Text.Encoding"/> to use for writing strings or
    /// <see langword="null"/> for using the default 1252 codepage.</param>
    /// <param name="cryptTail">Whether to encrypt trailing buffer data.</param>
    /// <param name="leaveOpen">Whether to leave the <see cref="Stream"/> open after disposing this instance.</param>
    public PbdfWriter(Stream baseStream, uint key, int bufferSize, int offsetCount, Encoding? encoding = null,
        bool cryptTail = true, bool leaveOpen = false)
    {
        // Validate base stream.
        if (!baseStream.CanWrite)
            throw new ArgumentException("Base stream requires to be writable.", nameof(baseStream));
        if (!baseStream.CanSeek)
            throw new ArgumentException("Base stream requires to be seekable.", nameof(baseStream));
        _baseStream = baseStream;
        _leaveOpen = leaveOpen;

        // Validate key.
        Key = key;

        // Validate buffer size.
        BufferSize = bufferSize;
        _buffer = new byte[BufferSize];
        _bufferPos = sizeof(uint)/*file size*/ + sizeof(uint)/*offset count*/ + offsetCount * sizeof(uint);
        CryptTail = cryptTail;

        // Validate offsets.
        _offsets = new int[offsetCount];

        // Validate encoding.
        Encoding = encoding ?? Encodings.Codepage1252;
    }

    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public override bool CanRead => false;

    public override bool CanSeek => false;

    public override bool CanWrite => true;

    public override long Length => throw new NotSupportedException("PBDF length cannot be retrieved.");

    public override long Position
    {
        get => throw new NotSupportedException("PBDF position cannot be retrieved.");
        set => throw new NotSupportedException("PBDF can only be seeked to offsets.");
    }

    /// <summary>
    /// Gets the size of a block at which a checksum follows.
    /// </summary>
    public int BufferSize { get; }

    /// <summary>
    /// Gets or sets a value indicating whether trailing buffer data will be encrypted.
    /// </summary>
    public bool CryptTail { get; set; }

    /// <summary>
    /// Gets or sets the <see cref="System.Text.Encoding"/> to use for writing strings.
    /// </summary>
    public Encoding Encoding { get; set; }

    /// <summary>
    /// Gets the encryption key with which contents of the file are encrypted.
    /// </summary>
    public uint Key { get; }

    /// <summary>
    /// Gets the number offsets in the PBDF that have to be satisfied through the <see cref="Offset"/> method.
    /// </summary>
    public int OffsetCount => _offsets.Length;

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public override void Flush()
    {
        // Validate offsets.
        if (_offsetIndex != _offsets.Length)
            throw new IOException($"{_offsets.Length - _offsetIndex} PBDF offsets have not been satisfied.");

        // Write a pending buffer.
        int dataSize = (int)_baseStream.Position + _bufferPos;
        if (_bufferPos > 0)
            SaveBuffer();

        // Update header.
        _baseStream.Seek(0, SeekOrigin.Begin);
        _baseStream.WriteUInt32((uint)_baseStream.Length);
        _baseStream.WriteInt32(_offsets.Length);
        foreach (int offset in _offsets)
            _baseStream.WriteInt32(offset);

        // Encrypt buffers.
        _baseStream.Seek(0, SeekOrigin.Begin);
        for (int bufferIndex = 0; bufferIndex < _baseStream.Length / BufferSize; bufferIndex++)
        {
            _baseStream.ReadExactly(_buffer);
            uint checksum = 0;
            if (bufferIndex != 0 && (Key == 0x5CA8 || Key == 0xD13F))
            {
                // Special encryption with specific keys in second and later buffers.
                for (int i = 0; i < BufferSize - sizeof(uint); i += sizeof(uint))
                {
#pragma warning disable CS8509 // Warning for non-exhaustive cases is incorrect as it does not handle arithmetics
                    Span<byte> bufferDword = _buffer.AsSpan(i);
                    uint decDword = BinaryPrimitives.ReadUInt32LittleEndian(bufferDword);
                    if (CryptTail || bufferIndex * BufferSize + i < dataSize)
                    {
                        uint keyDword = decDword >> 16 & 3 switch
                        {
                            0 => decDword - 0x50A4A89D,
                            1 => 0x3AF70BC4 - decDword,
                            2 => decDword - 0x07091971 << 1,
                            3 => 0x11E67319 - decDword << 1
                        };
                        uint encDword = decDword & 3 switch
                        {
                            0 => ~decDword ^ keyDword,
                            1 => ~decDword ^ ~keyDword,
                            2 => decDword ^ ~keyDword,
                            3 => decDword ^ keyDword ^ 0xFFFF
                        };
                        BinaryPrimitives.WriteUInt32LittleEndian(bufferDword, encDword);
                    }
                    else
                    {
                        decDword ^= Key;
                    }
                    checksum += decDword;
#pragma warning restore CS8509
                }
            }
            else
            {
                // Simple encryption for all buffers with most keys.
                for (int i = 0; i < BufferSize - sizeof(uint); i += sizeof(uint))
                {
                    Span<byte> bufferDword = _buffer.AsSpan(i);
                    uint decDword = BinaryPrimitives.ReadUInt32LittleEndian(bufferDword);
                    if (CryptTail || bufferIndex * BufferSize + sizeof(uint) < dataSize)
                        BinaryPrimitives.WriteUInt32LittleEndian(bufferDword, decDword ^ Key);
                    else
                        decDword ^= Key;
                    checksum += decDword;
                }
            }
            // Write the checksum and buffer.
            BinaryPrimitives.WriteUInt32LittleEndian(_buffer.AsSpan(BufferSize - sizeof(uint)), checksum);
            _baseStream.Seek(-BufferSize, SeekOrigin.Current);
            _baseStream.Write(_buffer);
        }
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
        throw new NotSupportedException($"{nameof(PbdfWriter)} is write-only, use a {nameof(PbdfReader)} instead.");
    }

    public override long Seek(long offset, SeekOrigin origin)
    {
        throw new NotSupportedException($"PBDF can only be seeked with the {nameof(Offset)}() method.");
    }

    public override void SetLength(long value)
    {
        throw new NotSupportedException("PBDF length cannot be set.");
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
        Write(buffer.AsSpan(offset, count));
    }

    public override void Write(ReadOnlySpan<byte> buffer)
    {
        int pos = 0;
        while (pos < buffer.Length)
        {
            int bytesRemain = BufferSize - sizeof(uint) - _bufferPos;
            if (bytesRemain > 0)
            {
                int size = Math.Min(bytesRemain, buffer.Length - pos);
                buffer.Slice(pos, size).CopyTo(_buffer.AsSpan(_bufferPos));
                pos += size;
                _bufferPos += size;
            }
            else
            {
                SaveBuffer();
            }
        }
    }

    /// <summary>
    /// Adds the file offset to the PBDF offset list.
    /// </summary>
    public void Offset()
    {
        if (_offsetIndex == _offsets.Length)
            throw new IOException("No more PBDF offsets available.");
        _offsets[_offsetIndex++] = (int)(_baseStream.Position * BufferSize + _bufferPos);
    }

    // ---- METHODS (PROTECTED) ----------------------------------------------------------------------------------------

    protected override void Dispose(bool disposing)
    {
        if (disposing && !_leaveOpen)
            _baseStream.Dispose();
        base.Dispose(disposing);
    }

    // ---- METHODS (PRIVATE) ------------------------------------------------------------------------------------------

    private void SaveBuffer()
    {
        // Simply write raw data, checksum and encryption must be done on Flush.
        _baseStream.Write(_buffer);
        _bufferPos = 0;
    }
}

using Syroot.PodTools.Core;

namespace Syroot.PodTools.Ghosts;

/// <summary>
/// Represents the contents of an internal sequence file (.seq).
/// </summary>
public class SequenceFile
{
    // ---- CONSTANTS --------------------------------------------------------------------------------------------------

    private const uint _magic1 = 0x12345678;
    private const uint _magic2 = 0x78451236;
    private const int _version = 2;

    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public IList<SequenceHeader> Headers { get; set; } = [];

    public SequenceInfo Info { get; set; } = new();

    public IList<SequencePoint> Points { get; set; } = [];

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static SequenceFile Load(Stream stream)
    {
        SequenceFile sequence = new();

        if (stream.ReadUInt32() != _magic1)
            throw new InvalidDataException("Invalid sequence file first magic value.");
        if (stream.ReadUInt32() != _magic2)
            throw new InvalidDataException("Invalid sequence file second magic value.");
        if (stream.ReadInt32() != _version)
            throw new InvalidDataException("Invalid sequence file version.");
        int dataEnd = stream.ReadInt32();
        int dataStart = stream.ReadInt32();

        // Load headers.
        int headerCount = stream.ReadInt32();
        int unknown = stream.ReadInt32(); // -1
        for (int i = 0; i < headerCount; ++i)
        {
            SequenceHeader header = new();
            header.Type = stream.ReadInt32();
            header.Data = new byte[stream.ReadInt32()];
            stream.ReadExactly(header.Data);
            sequence.Headers.Add(header);
        }

        // Load info.
        int infoSize = stream.ReadInt32();
        sequence.Info.Unknown0 = stream.ReadInt32();
        sequence.Info.CircuitIndex = stream.ReadInt32();
        sequence.Info.Time = stream.ReadFixed();
        for (int i = 0; i < 7; i++)
            sequence.Info.Unknown1[i] = stream.ReadInt32();
        sequence.Info.RaceTime = stream.ReadFixed();
        sequence.Info.StartTime = stream.ReadFixed();
        for (int l = 0; l < 3; l++)
            sequence.Info.LapTimes[l] = stream.ReadFixed();
        for (int l = 0; l < 3; l++)
            for (int p = 0; p < 4; p++)
                sequence.Info.PartTimes[l][p] = stream.ReadFixed();
        for (int i = 0; i < 6; i++)
            sequence.Info.Unknown2[i] = stream.ReadInt32();
        sequence.Info.VehicleName = stream.ReadStringBuf(8, Encodings.Codepage1252);
        for (int i = 0; i < 7; i++)
            sequence.Info.Unknown3[i] = stream.ReadInt32();
        sequence.Info.PlayerName = stream.ReadStringBuf(8, Encodings.Codepage1252);
        stream.ReadExactly(sequence.Info.Unknown4);
        sequence.Info.Grip = (byte)(stream.ReadUInt8() ^ 0x7C);
        sequence.Info.Speed = (byte)(stream.ReadUInt8() ^ 0xBD);
        sequence.Info.Handling = (byte)(stream.ReadUInt8() ^ 0x3A);
        sequence.Info.Brakes = (byte)(stream.ReadUInt8() ^ 0xE0);
        sequence.Info.Accel = (byte)(stream.ReadUInt8() ^ 0x1E);
        sequence.Info.Unknown5 = stream.ReadUInt8();
        for (int i = 0; i < 5; i++)
            sequence.Info.Unknown6[i] = stream.ReadInt32();

        // Load points.
        while (stream.Position < stream.Length)
        {
            SequencePoint point;
            point.Time = stream.ReadFixed();
            point.WheelRotationX = stream.ReadInt32();
            point.WheelRotationY = stream.ReadInt32();
            point.WheelZ0 = stream.ReadFixed();
            point.WheelZ1 = stream.ReadFixed();
            point.WheelZ2 = stream.ReadFixed();
            point.WheelZ3 = stream.ReadFixed();
            point.Position = stream.ReadVec3Fixed();
            point.RotationX = stream.ReadVec3Fixed();
            point.RotationY = stream.ReadVec3Fixed();
            sequence.Points.Add(point);
        }

        return sequence;
    }

    public void Save(Stream stream)
    {
        stream.WriteUInt32(_magic1);
        stream.WriteUInt32(_magic2);
        stream.WriteInt32(_version);
        stream.WriteInt32(316); // dataEnd
        stream.WriteInt32(60); // dataStart

        // Save headers.
        stream.WriteInt32(Headers.Count);
        stream.WriteInt32(-1);
        foreach (SequenceHeader header in Headers)
        {
            stream.WriteInt32(header.Type);
            stream.WriteInt32(header.Data.Length);
            stream.Write(header.Data);
        }

        // Save info.
        stream.WriteInt32(252); // infoSize
        stream.WriteInt32(Info.Unknown0);
        stream.WriteInt32(Info.CircuitIndex);
        stream.WriteFixed(Info.Time);
        for (int i = 0; i < 7; i++)
            stream.WriteInt32(Info.Unknown1[i]);
        stream.WriteFixed(Info.RaceTime);
        stream.WriteFixed(Info.StartTime);
        for (int l = 0; l < 3; l++)
            stream.WriteFixed(Info.LapTimes[l]);
        for (int l = 0; l < 3; l++)
            for (int p = 0; p < 4; p++)
                stream.WriteFixed(Info.PartTimes[l][p]);
        for (int i = 0; i < 6; i++)
            stream.WriteInt32(Info.Unknown2[i]);
        stream.WriteStringBuf(Info.VehicleName, 8, Encodings.Codepage1252);
        for (int i = 0; i < 7; i++)
            stream.WriteInt32(Info.Unknown3[i]);
        stream.WriteStringBuf(Info.PlayerName, 8, Encodings.Codepage1252);
        stream.Write(Info.Unknown4);
        stream.WriteUInt8((byte)(Info.Grip ^ 0x7C));
        stream.WriteUInt8((byte)(Info.Speed ^ 0xBD));
        stream.WriteUInt8((byte)(Info.Handling ^ 0x3A));
        stream.WriteUInt8((byte)(Info.Brakes ^ 0xE0));
        stream.WriteUInt8((byte)(Info.Accel ^ 0x1E));
        stream.WriteUInt8(Info.Unknown5);
        for (int i = 0; i < 5; i++)
            stream.WriteInt32(Info.Unknown6[i]);

        // Save points.
        foreach (SequencePoint point in Points)
        {
            stream.WriteFixed(point.Time);
            stream.WriteInt32(point.WheelRotationX);
            stream.WriteInt32(point.WheelRotationY);
            stream.WriteFixed(point.WheelZ0);
            stream.WriteFixed(point.WheelZ1);
            stream.WriteFixed(point.WheelZ2);
            stream.WriteFixed(point.WheelZ3);
            stream.WriteVec3Fixed(point.Position);
            stream.WriteVec3Fixed(point.RotationX);
            stream.WriteVec3Fixed(point.RotationY);
        }
    }
}

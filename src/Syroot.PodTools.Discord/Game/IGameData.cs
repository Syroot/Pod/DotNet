using System.Diagnostics.CodeAnalysis;
using Syroot.PodTools.Images;
using Syroot.PodTools.CircuitScripts;
using Syroot.PodTools.VehicleScripts;

namespace Syroot.PodTools.Discord;

public interface IGameData
{
    // ---- METHODS ----------------------------------------------------------------------------------------------------

    ImageFile LoadImage(string name);

    bool TryGetCircuitInfo(string name, [NotNullWhen(true)] out CircuitInfo? info);

    bool TryGetVehicleInfo(string name, [NotNullWhen(true)] out VehicleInfo? info);
}

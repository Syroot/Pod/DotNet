using Syroot.PodTools.Core;

namespace Syroot.PodTools.Images;

/// <summary>
/// Represents the contents of an image collection file (.img).
/// </summary>
public class ImageFile
{
    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public IList<Image> Images { get; set; } = [];

    public IList<Palette> Palettes { get; set; } = [];

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static ImageFile Load(Stream stream)
    {
        ImageFile result = new();

        int imageCount = stream.ReadInt32();
        int totalDataSize = stream.ReadInt32();
        int paletteCount = stream.ReadInt32();
        Span<int> imageDataOffsets = stackalloc int[imageCount];
        Span<short> imagePaletteIndices = stackalloc short[imageCount];

        for (int i = 0; i < imageCount; i++)
        {
            Image image = new();
            image.Width = stream.ReadInt16();
            image.Height = stream.ReadInt16();
            image.Id = stream.ReadInt32();
            imageDataOffsets[i] = stream.ReadInt32();
            imagePaletteIndices[i] = stream.ReadInt16();
            stream.Position += sizeof(short); // reserved
            result.Images.Add(image);
        }

        for (int i = 0; i < paletteCount; i++)
        {
            Palette palette = new();
            for (int j = 0; j < palette.Colors.Length; j++)
                palette.Colors[j] = stream.ReadRgb555();
            result.Palettes.Add(palette);
        }

        for (int i = 0; i < imageCount; i++)
        {
            if (imagePaletteIndices[i] != -1)
                result.Images[i].Palette = result.Palettes[imagePaletteIndices[i]];

            int imageDataSize = i == imageCount - 1
                ? totalDataSize - imageDataOffsets[i]
                : imageDataOffsets[i + 1] - imageDataOffsets[i];
            result.Images[i].Data = new byte[imageDataSize];
            stream.ReadExactly(result.Images[i].Data);
        }

        return result;
    }
}

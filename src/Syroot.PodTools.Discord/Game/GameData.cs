using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Options;
using Syroot.PodTools.Images;
using Syroot.PodTools.CircuitScripts;
using Syroot.PodTools.VehicleScripts;

namespace Syroot.PodTools.Discord;

public class GameData : IGameData
{
    // ---- FIELDS -----------------------------------------------------------------------------------------------------

    private readonly ILogger _logger;
    private readonly GameDataOptions _options;

    private readonly CircuitScript _circuitScript;
    private readonly VehicleScript _vehiclesScript;
    private readonly VehicleScript _vehicle2Script;

    // ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

    public GameData(
        IOptions<GameDataOptions> gameDataOptions,
        ILoggerFactory loggerFactory)
    {
        _logger = loggerFactory.CreateLogger("GameData");
        _options = gameDataOptions.Value;

        using (Stream stream = File.OpenRead(GetBinaryPath("scripts", "circuits.bin")))
            _circuitScript = CircuitScript.Load(stream);
        using (Stream stream = File.OpenRead(GetBinaryPath("scripts", "voitures.bin")))
            _vehiclesScript = VehicleScript.Load(stream);
        using (Stream stream = File.OpenRead(GetBinaryPath("scripts", "voiture2.bin")))
            _vehicle2Script = VehicleScript.Load(stream);
    }

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public ImageFile LoadImage(string name)
    {
        string filename = Path.ChangeExtension(name, "img");
        using Stream stream = File.OpenRead(GetBinaryPath("mimg", filename));
        return ImageFile.Load(stream);
    }

    public bool TryGetCircuitInfo(string name, [NotNullWhen(true)] out CircuitInfo? info)
    {
        info = _circuitScript.Infos.FirstOrDefault(x => x.LevName.Equals(name, StringComparison.OrdinalIgnoreCase));
        return info != null;
    }

    public bool TryGetVehicleInfo(string name, [NotNullWhen(true)] out VehicleInfo? info)
    {
        info = _vehiclesScript.Infos.FirstOrDefault(x => x.VehicleName.Equals(name, StringComparison.OrdinalIgnoreCase))
            ?? _vehicle2Script.Infos.FirstOrDefault(x => x.VehicleName.Equals(name, StringComparison.OrdinalIgnoreCase));
        return info != null;
    }

    // ---- METHODS (PRIVATE) ------------------------------------------------------------------------------------------

    private string GetBinaryPath(string folder, string fileName)
    {
        string path = Path.Combine(_options.Path, "data", "binary", folder, fileName);
        _logger.LogInformation("Requested binary file path \"{Path}\".", path);
        return path;
    }
}

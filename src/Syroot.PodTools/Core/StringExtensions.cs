﻿namespace Syroot.PodTools.Core;

public static class StringExtensions
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static string ToTitle(this string str)
    {
        return str.Length < 1 ? str : char.ToUpperInvariant(str[0]) + str[1..];
    }
}

using Syroot.PodTools.Core;

namespace Syroot.PodTools.VehicleScripts;

/// <summary>
/// Represents the contents of vehicle listing files (voiture.bin, voiture2.bin, voireset.bin).
/// </summary>
public class VehicleScript
{
    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public IList<VehicleInfo> Infos { get; set; } = [];

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static VehicleScript Load(Stream stream)
    {
        VehicleScript script = new();

        int infoCount = stream.ReadInt32();
        Span<byte> buffer = stackalloc byte[128];
        for (int i = 0; i < infoCount; i++)
        {
            stream.ReadExactly(buffer);
            for (int j = 0; j < buffer.Length; j++)
                buffer[j] ^= 0x68;

            script.Infos.Add(new()
            {
                Name = Encodings.Codepage1252.GetStringBuf(buffer.Slice(0, 64)),
                VehicleName = Encodings.Codepage1252.GetStringBuf(buffer.Slice(64, 20)),
                TgaName = Encodings.Codepage1252.GetStringBuf(buffer.Slice(84, 13)),
                SmallEName = Encodings.Codepage1252.GetStringBuf(buffer.Slice(97, 13)),
                SmallAName = Encodings.Codepage1252.GetStringBuf(buffer.Slice(110, 13)),
            });
        }

        return script;
    }
}

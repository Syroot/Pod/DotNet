﻿using Syroot.PodTools.Core;
using Syroot.PodTools.Pbdf;

namespace Syroot.PodTools.Pbdf;

/// <summary>
/// Represents extension methods for <see cref="PbdfWriter"/> instances.
/// </summary>
public static class PbdfWriterExtensions
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    /// <summary>
    /// Writes a <see cref="string"/> instance as a buffer of the given <paramref name="length"/>.
    /// </summary>
    /// <param name="pbdf">A <see cref="PbdfWriter"/> to write with.</param>
    /// <param name="length">The size of the buffer the string will be stored in.</param>
    public static void WriteStringBuf(this PbdfWriter pbdf, string value, int length)
    {
        pbdf.WriteStringBuf(value, length, pbdf.Encoding);
    }

    /// <summary>
    /// Writes a <see cref="string"/> as an encrypted string.
    /// </summary>
    /// <param name="pbdf">A <see cref="PbdfWriter"/> to write with.</param>
    /// <param name="value">A <see cref="string"/> instance to write.</param>
    public static void WriteStringEnc(this PbdfWriter pbdf, string value)
    {
        Span<byte> buffer = stackalloc byte[value.Length];
        pbdf.WriteUInt8((byte)pbdf.Encoding.GetBytes(value, buffer));
        for (int i = 0; i < buffer.Length; i++)
            buffer[i] = (byte)(buffer[i] ^ ~i);
        pbdf.Write(buffer);
    }
}

using System.Buffers.Binary;
using System.Drawing;
using System.Text;

namespace Syroot.PodTools.Core;

/// <summary>
/// Represents extension methods for <see cref="Stream"/> instances. All operations perform in little endianness.
/// </summary>
public static class StreamExtensions
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    /// <summary>
    /// Reads a <see cref="bool"/> instance as a 4-byte sized value.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="bool"/> instance.</returns>
    public static bool ReadBoolean4(this Stream stream)
    {
        return stream.ReadInt32() != 0;
    }

    /// <summary>
    /// Reads a <see cref="Color"/> instance as an RGB555 value.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="Color"/> instance.</returns>
    public static Color ReadRgb555(this Stream stream)
    {
        return Colors.Rgb555ToColor(stream.ReadUInt16());
    }

    /// <summary>
    /// Reads a <see cref="float"/> instance as a 16x16 fixed point decimal.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="float"/> instance.</returns>
    public static double ReadFixed(this Stream stream)
    {
        return stream.ReadInt32() / (double)(1 << 16);
    }

    /// <summary>
    /// Reads a <see cref="short"/> instance.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="short"/> instance.</returns>
    public static short ReadInt16(this Stream stream)
    {
        Span<byte> buffer = stackalloc byte[sizeof(short)];
        stream.ReadExactly(buffer);
        return BinaryPrimitives.ReadInt16LittleEndian(buffer);
    }

    /// <summary>
    /// Reads an <see cref="int"/> instance.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="int"/> instance.</returns>
    public static int ReadInt32(this Stream stream)
    {
        Span<byte> buffer = stackalloc byte[sizeof(int)];
        stream.ReadExactly(buffer);
        return BinaryPrimitives.ReadInt32LittleEndian(buffer);
    }

    /// <summary>
    /// Reads a <see cref="string"/> instance as a buffer of the given <paramref name="length"/>.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to read with.</param>
    /// <param name="length">The size of the buffer the string is stored in.</param>
    /// <param name="encoding">The <see cref="Encoding"/> to use for the string data.</param>
    /// <returns>The read <see cref="string"/> instance.</returns>
    public static string ReadStringBuf(this Stream stream, int length, Encoding encoding)
    {
        Span<byte> buffer = stackalloc byte[length];
        stream.ReadExactly(buffer);
        return encoding.GetStringBuf(buffer);
    }

    /// <summary>
    /// Reads a <see cref="byte"/> instance.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="byte"/> instance.</returns>
    public static byte ReadUInt8(this Stream stream)
    {
        Span<byte> buffer = stackalloc byte[sizeof(byte)];
        stream.ReadExactly(buffer);
        return buffer[0];
    }

    /// <summary>
    /// Reads an <see cref="ushort"/> instance.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="ushort"/> instance.</returns>
    public static ushort ReadUInt16(this Stream stream)
    {
        Span<byte> buffer = stackalloc byte[sizeof(ushort)];
        stream.ReadExactly(buffer);
        return BinaryPrimitives.ReadUInt16LittleEndian(buffer);
    }

    /// <summary>
    /// Reads an <see cref="uint"/> instance.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="uint"/> instance.</returns>
    public static uint ReadUInt32(this Stream stream)
    {
        Span<byte> buffer = stackalloc byte[sizeof(uint)];
        stream.ReadExactly(buffer);
        return BinaryPrimitives.ReadUInt32LittleEndian(buffer);
    }

    /// <summary>
    /// Reads a <see cref="Vec3"/> instance as 16x16 fixed point decimals.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to read with.</param>
    /// <returns>The read <see cref="Vec3"/> instance.</returns>
    public static Vec3 ReadVec3Fixed(this Stream stream)
    {
        return new(stream.ReadFixed(), stream.ReadFixed(), stream.ReadFixed());
    }

    /// <summary>
    /// Writes a <see cref="bool"/> instance as a 4-byte sized value.
    /// </summary>
    /// <param name="pbdf">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="bool"/> instance to write.</param>
    public static void WriteBoolean32(this Stream stream, bool value)
    {
        stream.WriteInt32(value ? 1 : 0);
    }

    /// <summary>
    /// Writes a <see cref="Color"/> instance as an RGB555 value.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="Color"/> instance to write.</param>
    public static void WriteRgb555(this Stream stream, Color value)
    {
        stream.WriteUInt16(Colors.ColorToRgb555(value));
    }

    /// <summary>
    /// Writes a <see cref="double"/> instance as a 16x16 fixed point decimal.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="double"/> instance to write.</param>
    public static void WriteFixed(this Stream stream, double value)
    {
        stream.WriteInt32((int)(value * (1 << 16)));
    }

    /// <summary>
    /// Writes a <see cref="short"/> instance.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="short"/> instance to write.</param>
    public static void WriteInt16(this Stream stream, short value)
    {
        Span<byte> buffer = stackalloc byte[sizeof(short)];
        BinaryPrimitives.WriteInt16LittleEndian(buffer, value);
        stream.Write(buffer);
    }

    /// <summary>
    /// Writes an <see cref="int"/> instance.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="int"/> instance to write.</param>
    public static void WriteInt32(this Stream stream, int value)
    {
        Span<byte> buffer = stackalloc byte[sizeof(int)];
        BinaryPrimitives.WriteInt32LittleEndian(buffer, value);
        stream.Write(buffer);
    }

    /// <summary>
    /// Writes a <see cref="string"/> instance as a buffer of the given <paramref name="length"/>.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="string"/> instance to write.</param>
    /// <param name="length">The size of the buffer the string will be stored in.</param>
    /// <param name="encoding">The <see cref="Encoding"/> to use for the string data.</param>
    public static void WriteStringBuf(this Stream stream, string value, int length, Encoding encoding)
    {
        Span<byte> buffer = stackalloc byte[length];
        encoding.GetBytes(value, buffer);
        stream.Write(buffer);
    }

    /// <summary>
    /// Writes a <see cref="byte"/> instance.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="byte"/> instance to write.</param>
    public static void WriteUInt8(this Stream stream, byte value)
    {
        stream.Write([value]);
    }

    /// <summary>
    /// Writes an <see cref="ushort"/> instance.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="ushort"/> instance to write.</param>
    public static void WriteUInt16(this Stream stream, ushort value)
    {
        Span<byte> buffer = stackalloc byte[sizeof(ushort)];
        BinaryPrimitives.WriteUInt16LittleEndian(buffer, value);
        stream.Write(buffer);
    }

    /// <summary>
    /// Writes an <see cref="uint"/> instance.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="uint"/> instance to write.</param>
    public static void WriteUInt32(this Stream stream, uint value)
    {
        Span<byte> buffer = stackalloc byte[sizeof(uint)];
        BinaryPrimitives.WriteUInt32LittleEndian(buffer, value);
        stream.Write(buffer);
    }

    /// <summary>
    /// Writes a <see cref="Vec3"/> instance as 16x16 fixed point decimals.
    /// </summary>
    /// <param name="stream">A <see cref="Stream"/> to write with.</param>
    /// <param name="value">A <see cref="Vec3"/> instance to write.</param>
    public static void WriteVec3Fixed(this Stream stream, Vec3 value)
    {
        stream.WriteFixed(value.X);
        stream.WriteFixed(value.Y);
        stream.WriteFixed(value.Z);
    }
}

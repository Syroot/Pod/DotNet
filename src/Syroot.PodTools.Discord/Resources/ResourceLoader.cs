using System.Reflection;

namespace Syroot.PodTools.Discord.Resources;

public sealed class ResourceLoader : IResourceLoader
{
    // ---- FIELDS -----------------------------------------------------------------------------------------------------

    private readonly Assembly _assembly = Assembly.GetExecutingAssembly();

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public byte[] GetData(string name)
    {
        foreach (string resourceName in _assembly.GetManifestResourceNames())
        {
            if (resourceName.EndsWith(name))
            {
                using Stream stream = _assembly.GetManifestResourceStream(resourceName)
                    ?? throw new InvalidDataException($"Could not open resource '{name}'.");
                byte[] data = new byte[stream.Length];
                stream.ReadExactly(data);
                return data;
            }
        }

        throw new InvalidDataException($"Could not find resource '{name}'.");
    }
}

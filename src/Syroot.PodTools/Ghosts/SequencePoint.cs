﻿using Syroot.PodTools.Core;

namespace Syroot.PodTools.Ghosts;

public struct SequencePoint
{
    public double Time;
    public int WheelRotationX;
    public int WheelRotationY;
    public double WheelZ0;
    public double WheelZ1;
    public double WheelZ2;
    public double WheelZ3;
    public Vec3 Position;
    public Vec3 RotationX;
    public Vec3 RotationY;
}

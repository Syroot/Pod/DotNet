using SkiaSharp;
using Syroot.PodTools.Images;
using Syroot.PodTools.CircuitScripts;

namespace Syroot.Pod.Scratchpad;

internal class Program
{
    private const string _path = @"C:\Users\Ray\Archive\Games\Pod\_platinum";

    private static CircuitScript _circuitScript = null!;

    private static void Main()
    {
        using (Stream stream = File.OpenRead(GetBinaryPath("scripts", "circuits.bin")))
            _circuitScript = CircuitScript.Load(stream);

        string directory = $@"C:\Users\Ray\Downloads\img";
        Directory.CreateDirectory(directory);

        foreach (CircuitInfo info in _circuitScript.Infos)
        {
            ImageFile imageFile = LoadImage(info.Name);
            using SKImage image = imageFile.GetSKImage(0);
            File.WriteAllBytes(Path.Combine(directory, $"{info.LevName}.png"),
                image.Encode(SKEncodedImageFormat.Png, 100).ToArray());
        }
    }

    private static string GetBinaryPath(string folder, string fileName)
    {
        string path = Path.Combine(_path, "data", "binary", folder, fileName);
        return path;
    }

    private static ImageFile LoadImage(string name)
    {
        string filename = Path.ChangeExtension(name, "img");
        using Stream stream = File.OpenRead(GetBinaryPath("mimg", filename));
        return ImageFile.Load(stream);
    }
}

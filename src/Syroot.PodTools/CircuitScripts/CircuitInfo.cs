namespace Syroot.PodTools.CircuitScripts;

public class CircuitInfo
{
    public string Name { get; set; } = "";
    public string LevName { get; set; } = "";
    public string TgaName { get; set; } = "";
    public string Scotch { get; set; } = "";
    public string ScotchMirror { get; set; } = "";
    public CircuitFlags Flags { get; set; }
    public byte Version { get; set; }
    public byte Id { get; set; }
    public int Length { get; set; } // in meters
    public byte Laps { get; set; }
    public byte Level { get; set; }
}

[Flags]
public enum CircuitFlags : byte
{
    None,
    Visible = 1 << 0,
    CanMirror = 1 << 1,
}

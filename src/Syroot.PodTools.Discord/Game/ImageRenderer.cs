using System.Collections.Concurrent;
using SkiaSharp;
using Syroot.PodTools.Discord.Resources;
using Syroot.PodTools.Images;

namespace Syroot.PodTools.Discord;

public sealed class ImageRenderer(
    IGameData _gameData,
    IResourceLoader _resourceLoader)
    : IDisposable, IImageRenderer
{
    // ---- FIELDS -----------------------------------------------------------------------------------------------------

    private static readonly SKPaint s_mirrorPaint = new()
    {
        ColorFilter = SKColorFilter.CreateColorMatrix(
        [
            0, 1, 0, 0, 0,
            1, 0, 0, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 0, 1, 0,
        ])
    };

    private readonly ConcurrentDictionary<string, byte[]> _cache = [];

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public void Dispose()
    {
        s_mirrorPaint.ColorFilter.Dispose();
        s_mirrorPaint.Dispose();
    }

    public byte[] GetCircuitImage(string name, bool mirror)
    {
        string key = mirror ? string.Concat(name.Reverse()) : name;
        return _cache.GetOrAdd(key, _ =>
        {
            ImageFile circuitImage = _gameData.LoadImage(name);
            using SKImage image = circuitImage.GetSKImage(circuitImage.Images[0].Id);
            if (mirror)
            {
                using SKBitmap canvasBitmap = new(image.Width, image.Height);
                using SKCanvas canvas = new(canvasBitmap);
                canvas.Translate(image.Width, 0);
                canvas.Scale(-1, 1);
                canvas.DrawImage(image, SKPoint.Empty, s_mirrorPaint);
                return canvasBitmap.Encode(SKEncodedImageFormat.Png, 100).ToArray();
            }
            else
            {
                return image.Encode().ToArray();
            }
        });
    }

    public byte[] GetVehicleImage(string name)
    {
        return _cache.GetOrAdd(name, _ => _resourceLoader.GetData($"Cars.{name}.png"));
    }
}

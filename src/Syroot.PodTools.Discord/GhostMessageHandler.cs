using System.Text;
using System.Xml.Linq;
using Discord;
using Discord.WebSocket;
using Syroot.PodTools.Ghosts;

namespace Syroot.PodTools.Discord;

public class GhostMessageHandler(
    IGhostDetailsExtractor _ghostDetailsExtractor,
    HttpClient _httpClient,
    IImageRenderer _imageRenderer,
    ILiveSplitExporter _liveSplitExporter,
    ILogger<GhostMessageHandler> _logger)
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public async Task HandleMessageAsync(SocketMessage message)
    {
        if (message.Author.IsBot)
            return;

        // Handle all ghost attachments.
        Attachment[] ghostAttachments = [.. message.Attachments
            .Where(x => Path.GetExtension(x.Filename).Equals(".GHT", StringComparison.InvariantCultureIgnoreCase))];
        await Parallel.ForEachAsync(ghostAttachments, async (attachment, ct) =>
        {
            await HandleAttachment(message, attachment);
        });

        // Delete original message if it has no other attachments.
        if (ghostAttachments.Length > 0 && ghostAttachments.Length == message.Attachments.Count)
            await message.Channel.DeleteMessageAsync(message);
    }

    // ---- METHODS (PRIVATE) ------------------------------------------------------------------------------------------

    private static void BuildEmbedContent(EmbedBuilder embed, SequenceInfo info, GhostDetails details)
    {
        static string formatKeycap(char character) => $"{character}\uFE0F\u20E3";

        // Append title and car settings.
        string circuitIcon = details.CircuitUnknown ? "⚠️ " : "";
        string circuitLevName = details.CircuitMirror ? string.Concat(details.CircuitLevName.Reverse()) : details.CircuitLevName;
        embed.WithTitle($"{info.PlayerName} - {circuitIcon}{circuitLevName} {FormatTime(info.RaceTime)}");

        string vehicleIcon = details.VehicleUnknown ? "⚠️ " : "";
        string stats = string.Join('-', info.Accel, info.Brakes, info.Grip, info.Handling, info.Speed);
        string statsIcon = details.VehiclePointsCheat ? "⚠️ " : "";
        embed.AddField($"{vehicleIcon}{details.VehicleName}", $"{statsIcon}{stats} ({details.VehiclePointsTotal})");

        // Append times.
        string[] partTexts = new string[details.PartCount];
        for (int lap = 0; lap < 3; lap++)
        {
            double lapTime = info.LapTimes[lap];
            string lapIcon
                = lapTime == details.BestLapTime ? "⭐"
                : lapTime == details.WorstLapTime ? "❌"
                : "🔁";
            string lapText = $"{lapIcon} {FormatTime(lapTime)}";

            for (int part = 0; part < details.PartCount; part++)
            {
                double partTime = info.PartTimes[lap][part];
                string partIcon
                    = partTime == details.BestPartTimes[part] ? "⭐"
                    : partTime == details.WorstPartTimes[part] ? "❌"
                    : formatKeycap((char)('1' + part));
                partTexts[part] = $"`{partIcon} {FormatTime(partTime)}`";
            }
            embed.AddField(lapText, string.Join('\n', partTexts), true);
        }

        // Append additional times.
        embed.AddField("Start Delay", FormatTime(info.StartTime), true);
        embed.AddField("Average Lap", FormatTime(details.AverageLapTime), true);
        embed.AddField("Virtual Lap", FormatTime(details.VirtualLapTime), true);
    }

    private static string FormatLssFileName(SequenceInfo info, GhostDetails details)
    {
        string circuit = details.CircuitMirror ? string.Concat(details.CircuitLevName.Reverse()) : details.CircuitLevName;
        string time = FormatTime(info.RaceTime).Replace('\'', '_').Replace('"', '_');
        return Path.ChangeExtension($"{circuit}__{time}__{info.PlayerName}", "lss");
    }

    private static string FormatTime(double time)
    {
        time = Math.Round(time, 3, MidpointRounding.AwayFromZero);
        int min = (int)(time / 60);
        int sec = (int)(time % 60);
        int mil = (int)Math.Round(time % 1 * 1000, 0, MidpointRounding.AwayFromZero);
        return $"{min:00}'{sec:00}\"{mil:000}";
    }

    private async Task HandleAttachment(SocketMessage message, Attachment attachment)
    {
        // Load ghost.
        _logger.LogInformation("Received ghost {Url}.", attachment.Url);
        GhostFile ghost;
        using (Stream stream = await _httpClient.GetStreamAsync(attachment.Url))
            ghost = GhostFile.Load(stream);

        try
        {
            await Parallel.ForEachAsync(ghost.Sequences, async (sequence, ct) =>
            {
                await HandleSequence(message, attachment.Filename, sequence);
            });
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Cannot process ghost {Url}.", attachment.Url);
            await message.Channel.SendMessageAsync(message.Author.Mention + " Sorry, I don't understand this ghost.");
        }
    }

    private async Task HandleSequence(SocketMessage message, string ghostFileName, SequenceFile sequence)
    {
        // Build reply.
        EmbedBuilder embed = new();
        embed.WithAuthor(message.Author);
        embed.WithColor(new Color(168, 67, 0));
        embed.WithFooter("Pod Ghost File");
        embed.WithTimestamp(message.Timestamp);
        if (!string.IsNullOrWhiteSpace(message.Content))
            embed.WithDescription($"\"{message.Content}\"");

        GhostDetails details = _ghostDetailsExtractor.Create(ghostFileName, sequence.Info);
        BuildEmbedContent(embed, sequence.Info, details);

        List<FileAttachment> attachments = [];
        try
        {
            // Attach GHT file of single sequence.
            GhostFile singleGhost = new();
            singleGhost.CircuitIndex = sequence.Info.CircuitIndex;
            singleGhost.Sequences.Add(sequence);

            FileAttachment attachment = new(new MemoryStream(), ghostFileName, ghostFileName);
            singleGhost.Save(attachment.Stream);
            attachment.Stream.Position = 0;
            attachments.Add(attachment);

            // Attach LSS file.
            XElement lss = _liveSplitExporter.Export(sequence.Info, details);
            string lssFileName = FormatLssFileName(sequence.Info, details);
            string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + lss.ToString();
            using MemoryStream lssStream = new(Encoding.UTF8.GetBytes(xml), false);
            attachments.Add(new(lssStream, lssFileName, lssFileName));

            // Attach circuit image.
            if (!details.CircuitUnknown)
            {
                MemoryStream stream = new(_imageRenderer.GetCircuitImage(details.CircuitName, details.CircuitMirror), false);
                attachments.Add(new(stream, $"{details.CircuitName}.png", details.CircuitName));
                embed.WithImageUrl($"attachment://{details.CircuitName}.png");
            }

            // Attach vehicle image.
            if (!details.VehicleUnknown)
            {
                MemoryStream stream = new(_imageRenderer.GetVehicleImage(details.VehicleName), false);
                attachments.Add(new(stream, $"{details.VehicleName}.png", details.VehicleName));
                embed.WithThumbnailUrl($"attachment://{details.VehicleName}.png");
            }

            // Send message.
            await message.Channel.SendFilesAsync(attachments, embed: embed.Build());
        }
        finally
        {
            foreach (FileAttachment fileAttachment in attachments)
                fileAttachment.Dispose();
        }
    }
}

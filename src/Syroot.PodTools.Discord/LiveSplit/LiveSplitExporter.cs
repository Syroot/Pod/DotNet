using System.Buffers.Binary;
using System.Xml.Linq;
using Syroot.PodTools.Discord.Resources;
using Syroot.PodTools.Ghosts;

namespace Syroot.PodTools.Discord;

public sealed class LiveSplitExporter : ILiveSplitExporter
{
    // ---- FIELDS -----------------------------------------------------------------------------------------------------

    private readonly IImageRenderer _imageRenderer;
    private readonly IResourceLoader _resourceLoader;

    private readonly byte[] _imageHeaderData;
    private readonly string _gameIconCData;

    // ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

    public LiveSplitExporter(
        IImageRenderer imageRenderer,
        IResourceLoader resourceLoader)
    {
        _imageRenderer = imageRenderer;
        _resourceLoader = resourceLoader;

        _imageHeaderData = _resourceLoader.GetData("LssImageHeader.bin");
        _gameIconCData = SerializeImage(_resourceLoader.GetData("GameIcon.png"));
    }

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public XElement Export(SequenceInfo info, GhostDetails details)
    {
        // Load circuit icon.
        byte[]? circuitIcon = details.CircuitUnknown ? null
            : _imageRenderer.GetCircuitImage(details.CircuitName, details.CircuitMirror);

        // Build XML contents.
        XElement lss = new("Run", new XAttribute("version", "1.7.0"),
            new XElement("GameIcon", new XCData(_gameIconCData)),
            new XElement("GameName", "POD: Planet of Death"),
            new XElement("CategoryName"),
            new XElement("Offset", "00:00:00"),
            new XElement("AttemptCount", 0));

        double gameTime = 0;
        XElement segments = new("Segments");
        for (int lap = 0; lap < 3; ++lap)
        {
            for (int part = 0; part < details.PartCount; ++part)
            {
                string name = part == details.PartCount - 1
                    ? $"{{Lap {lap + 1}}} Part {part + 1}"
                    : $"-Part {part + 1}";
                gameTime += info.PartTimes[lap][part];

                segments.Add(new XElement("Segment",
                    new XElement("Name", name),
                    circuitIcon == null ? null : new XElement("Icon", new XCData(SerializeImage(circuitIcon))),
                    new XElement("SplitTimes",
                        new XElement("SplitTime", new XAttribute("name", "Personal Best"),
                            new XElement("GameTime", TimeSpan.FromSeconds(gameTime).ToString("c"))))));
            }
        }
        lss.Add(segments);

        lss.Add(new XElement("AutoSplitterSettings",
            new XElement("Version", "1.5"),
            new XElement("Start", "True"),
            new XElement("Reset", "True"),
            new XElement("Split", "True"),
            new XElement("CustomSettings",
                new XElement("Setting", new XAttribute("id", "splitLap"), new XAttribute("type", "bool"), "True"),
                new XElement("Setting", new XAttribute("id", "splitPart"), new XAttribute("type", "bool"), "True"))));

        return lss;
    }

    // ---- METHODS (PRIVATE) ------------------------------------------------------------------------------------------

    public string SerializeImage(byte[] imageData)
    {
        // Simulate BinaryFormatter serialization.
        byte[] data = new byte[_imageHeaderData.Length + 4 + 1 + imageData.Length + 1];

        // Write header.
        _imageHeaderData.AsSpan().CopyTo(data);

        // Write data.
        BinaryPrimitives.WriteInt32LittleEndian(data.AsSpan(_imageHeaderData.Length), imageData.Length);
        data[_imageHeaderData.Length + 4] = 0x2;
        imageData.CopyTo(data.AsSpan(_imageHeaderData.Length + 4 + 1));
        data[^1] = 0xB;

        // Convert to Base64.
        return Convert.ToBase64String(data);
    }
}

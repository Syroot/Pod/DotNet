using System.Buffers;
using Syroot.PodTools.Core;

namespace Syroot.PodTools.Ghosts;

/// <summary>
/// Represents the contents of a multi-sequence exported ghost file (.ght).
/// </summary>
public class GhostFile
{
    // ---- CONSTANTS --------------------------------------------------------------------------------------------------

    private const uint _magic1 = 0x456F1E9C;
    private const uint _magic2 = 0xFA48EFB4;
    private const int _version = 1;

    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public int CircuitIndex { get; set; } // dependent on client circuit order

    public IList<SequenceFile> Sequences { get; set; } = [];

    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static GhostFile Load(Stream stream)
    {
        GhostFile ghost = new();

        // Load header.
        if (stream.ReadInt32() != _version)
            throw new InvalidDataException("Invalid ghost file version.");
        if (stream.ReadUInt32() != _magic1)
            throw new InvalidDataException("Invalid ghost file first magic value.");
        if (stream.ReadUInt32() != _magic2)
            throw new InvalidDataException("Invalid ghost file second magic value.");
        int sequenceCount = stream.ReadInt32();
        ghost.CircuitIndex = stream.ReadInt32();

        // Load sequences.
        for (int i = 0; i < sequenceCount; i++)
        {
            double raceTime = stream.ReadFixed();
            int sequenceSize = stream.ReadInt32();

            byte[] buffer = ArrayPool<byte>.Shared.Rent(sequenceSize);
            stream.ReadExactly(buffer.AsSpan(0, sequenceSize));
            using MemoryStream sequenceStream = new(buffer, 0, sequenceSize, false);
            ghost.Sequences.Add(SequenceFile.Load(sequenceStream));
            ArrayPool<byte>.Shared.Return(buffer);
        }

        return ghost;
    }

    public void Save(Stream stream)
    {
        // Save header.
        stream.WriteInt32(_version);
        stream.WriteUInt32(_magic1);
        stream.WriteUInt32(_magic2);
        stream.WriteInt32(Sequences.Count);
        stream.WriteInt32(CircuitIndex);

        // Save sequences.
        foreach (SequenceFile sequence in Sequences)
        {
            stream.WriteFixed(sequence.Info.RaceTime);

            using MemoryStream sequenceStream = new();
            sequence.Save(sequenceStream);
            stream.WriteInt32((int)sequenceStream.Position);
            sequenceStream.Position = 0;
            sequenceStream.CopyTo(stream);
        }
    }
}

﻿using System.Text;

namespace Syroot.PodTools.Core;

/// <summary>
/// Represents extension methods for <see cref="Encoding"/> instances.
/// </summary>
public static class EncodingExtensions
{
    // ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

    public static string GetStringBuf(this Encoding encoding, ReadOnlySpan<byte> buffer)
    {
        int length = buffer.IndexOf((byte)0);
        if (length == -1)
            length = buffer.Length;
        return encoding.GetString(buffer[..length]);
    }
}

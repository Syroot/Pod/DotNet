using Syroot.PodTools.Ghosts;

namespace Syroot.PodTools.Discord;

/// <summary>
/// Represents a factory extracting additional details from ghost files.
/// </summary>
public interface IGhostDetailsExtractor
{
    // ---- METHODS ----------------------------------------------------------------------------------------------------

    GhostDetails Create(string filename, SequenceInfo info);
}

public class GhostDetails
{
    // ---- PROPERTIES -------------------------------------------------------------------------------------------------

    public string CircuitName { get; set; } = "";
    public string CircuitLevName { get; set; } = "";
    public bool CircuitMirror { get; set; }
    public bool CircuitUnknown { get; set; }
    public string VehicleName { get; set; } = "";
    public int VehiclePointsTotal { get; set; }
    public bool VehiclePointsCheat => VehiclePointsTotal > 300;
    public bool VehicleUnknown { get; set; }
    public double BestLapTime { get; set; }
    public double WorstLapTime { get; set; }
    public double AverageLapTime { get; set; }
    public double VirtualLapTime { get; set; }
    public int PartCount { get; set; }
    public double[] BestPartTimes { get; } = new double[4];
    public double[] WorstPartTimes { get; } = new double[4];
}

﻿using System.Xml.Linq;
using Syroot.PodTools.Ghosts;

namespace Syroot.PodTools.Discord;

/// <summary>
/// Represents a converter of <see cref="GhostFile"/> sequences to a LiveSplit LSS-compatible <see cref="XElement"/>.
/// </summary>
public interface ILiveSplitExporter
{
    // ---- METHODS ----------------------------------------------------------------------------------------------------

    XElement Export(SequenceInfo info, GhostDetails details);
}
